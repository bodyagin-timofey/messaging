package net.yoshkin.msg.pojo;

/**
 * Created by TiM on 15.07.2018.
 */
public class Message {
    private String text;

    public String getText() {
        return text;
    }

    public Message setText(String text) {
        this.text = text;
        return this;
    }
}
