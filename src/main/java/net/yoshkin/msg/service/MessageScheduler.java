package net.yoshkin.msg.service;

import net.yoshkin.msg.pojo.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by TiM on 15.07.2018.
 */
@Service
public class MessageScheduler {

    @Autowired
    private SimpMessagingTemplate webSocket;

    @Scheduled(fixedDelay = 10000)
    public void send() {
        webSocket.convertAndSend("/messages", new Message().setText("Кто здесь?"));
    }
}
