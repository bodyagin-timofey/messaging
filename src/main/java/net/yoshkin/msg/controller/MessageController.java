package net.yoshkin.msg.controller;

import net.yoshkin.msg.pojo.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by TiM on 15.07.2018.
 */
@Controller
public class MessageController {
    @MessageMapping("/message")
    @SendTo("/messages")
    public Message message(Message message) {
        return message;
    }
}
