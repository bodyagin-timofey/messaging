new Vue({
  el: '#app',
  data: function() {
    return {
      messages: [],
      message: ''
    }
  },
  methods: {
    send: function(){
      if (this.stomp && this.stomp.connected) {
        const msg = { text: this.message };
        this.stomp.send("/app/message", {}, JSON.stringify(msg));
      }
    },
    connect: function(){
      var that = this;
      this.socket = new SockJS("/socks")
      this.stomp = Stomp.over(this.socket);
      this.stomp.connect({},
        function(frame) {
          that.stomp.subscribe("/messages", function(tick) {
            that.messages.push(JSON.parse(tick.body).text);
            that.setFavicon('img/new.png');
          });
        },
        function(error) {
          console.log(error);
        }
      );
    },
    disconnect: function() {
      if (this.stomp) {
        this.stomp.disconnect();
      }
    },
    removeFavicon: function() {
      var links = document.getElementsByTagName ('link');
      var head = document.getElementsByTagName ('head')[0];
      for (var i = 0; i < links.length; i++) {
        if (links[i].getAttribute ('rel') === 'icon') {
          head.removeChild (links[i])
        }
      }
    },
    setFavicon: function(url) {
      this.removeFavicon();
      var link=document.createElement('link');
      link.type='image/x-icon';
      link.rel='icon';
      link.href=url;
      document.getElementsByTagName('head')[0].appendChild(link);
    }
  },
  mounted: function() {
    this.connect();
    that = this;
    window.addEventListener("focus", function(event) {
      that.setFavicon('/img/msg.png');
    }, false);
    that.setFavicon('/img/msg.png');
  },
  destroyed: function() {
    window.removeEventListener("focus");
  }
});